/******************************************************************************
* Assignment:  LAB11 
* Lab Section: Wednesday 15:30 SC189 
* Description: Prompts the user for a number, checking to make sure that it is
*              positive and prompting the user again if it is not. After 
*              retrieving a proper number, generates 10 consecutive primes
*              greater or equal to the number given by the user. Finally, 
*              prints primes from the array of 10 by going through the digits
*              of the last prime number and using the values of the digits
*              as indices to print from.
* Programmers: Vadim Nikiforov vnikifor@purdue.edu
*              Ziheng wang wang2194@purdue.edu
*              Haozheng Qu qu34@purdue.edu
******************************************************************************/
#include <stdio.h>
#include <math.h>

#define A_SIZE 10 // the size of the array of prime numbers
#define numDigits(a) log10(a) + 1 // evaluates to the number of digits in a number

// function declarations
long long getNum();
int checkIfPrime(long long);
void genArray(long long[], long long);
void printScrambled(long long[]);

int main(void)
{
  long long num; // the number the user enters
  long long primes[A_SIZE]; // the array of primes based on the user's number
  
  num = getNum(); // retrieve number from the user
  genArray(primes, num); // generate an array of primes based on the user's number
  printScrambled(primes); // prints elements of the prime array based on the last element

  return 0;
}

/******************************************************************************
* Function:    getNum
* Description: retrieves a number from the user and prompts them again if they
*              enter a value that is not positive
* Parameters:  void
* Return:      long long, the number that the user enters
******************************************************************************/
long long getNum()
{
  // local declarations
  long long answer; // the number the user inputs
  
  printf("Enter starting value:"); 
  scanf("%lld", &answer);
  // enters a loop if the user enters an improper number
  while(answer <= 0)
  {
    printf("\nError! Positive values only!!\n");
    printf("\nEnter starting value:");
    scanf("%lld", &answer);
  }

  return answer;
}


/******************************************************************************
* Function:    checkIfPrime
* Description: determines whether or not a number is prime
* Parameters:  test, long, the number to test whether no it is prime for
* Return:      int, whether or not the number is prime
******************************************************************************/
int checkIfPrime(long long test)
{
  // local declarations
  long largest; // the largest number to use as a divisor
  int isPrime; // whether or not the number is prime
  long tester; // the number to divide the prime by

  // prepare for the loop
  tester = 3; // 1 is accounted for as a special case in the outer loop, and 2 is not a sum of squares so it must not be tested for
  isPrime = 1; // first assume the number is prime
  largest = sqrt(test);
    
  while(isPrime && tester <= largest) // the moment it is obvious a number is not prime, stop
  {
    isPrime = test % tester != 0; // check for divisiblity
    tester += 2; // only check odd numbers
  }

  return isPrime;
}

/******************************************************************************
* Function:    genArray
* Description: generates an array of the next 10 prime numbers based on a given
*              number
* Parameters:  primeArray, long long[], an array of 10 prime numbers
*              start, long long, the number to start searching for primes from
* Return:      void
******************************************************************************/
void genArray(long long primeArray[], long long start)
{
  // local declarations
  int i; // counter for a for loop

  start += start % 2 == 0 && start != 2; // starts searching from the first odd number, unless the number is 2

  // searches for primes, checking if each conscutive odd number or 2 is prime
  for(i = 0; i < A_SIZE; i++)
  {
    // if the number is 2, then it must be a prime. Put it in the array, and add one to get to an odd number
    if(start <= 2)
    {
      primeArray[i] = start;
      start++;
    }
    // else, check if consecutive odd numbers are prime
    else
    {
      while(!checkIfPrime(start))
      {
        start += 2;
      }
      primeArray[i] = start;
      start += 2;
    }
  }

  return;
}

/******************************************************************************
* Function:    printScrambled
* Description: prints prime numbers from an array of primes, using the digit
*              values of the last element as the indices to print from
* Parameters:  primeArray, long long[], an array of prime numbers
* Return:      void
******************************************************************************/
void printScrambled(long long primeArray[])
{
  //local declarations
  int size; // the size of the last prime in the array
  int i; // counter for a for loop
  int index; // the index to select the prime number from
  long long last; // a copy by value of the last number in the prime array

  size = numDigits(primeArray[9]); // get the size of the last prime number
  last = primeArray[9]; // make a copy by value of the last prime number to make changes without changing the array

  printf("\nGenerated Results:");
  // print the numbers from the array by going through each digit value of the last prime number
  for(i = 0; i < size; i++)
  {
    index = last % 10;
    printf(" %lld", primeArray[index]);
    last = last / 10;
  }

  printf("\n");

  return;
}
